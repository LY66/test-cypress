describe('The Home Page', () => {
  it('successfully loads', () => {
    cy.visit('/') // change URL to match your dev URL

    cy.wait(5000)

    cy.get('.chart').toMatchImageSnapshot({
      imageConfig: {
        createDiffImage: true,
        threshold: 0
      }
    })
  })
})